<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {


    
    public function __construct()
    {
        parent::__construct();
        
    }
    
    public function index()
    {
        $this->load->view('index');
    }

    public function generateReport()
    {
        $post = $_POST;

        $this->load->model('questionario_model');
        $_SESSION['msg'] = $this->questionario_model->rankQuestionario('');
        

        $this->load->view('report');   
    }
}

/* End of file App.php */


?>