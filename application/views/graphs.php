<?php
$this->load->view('_index');
?>

<!-- Page Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h1 class="mt-5">Questionário LGPD</h1>
      <p class="text-muted h5">A LGPD é uma legislação que regula as atividades de tratamento de dados pessoais.
        <br />Ela entrará em efeito em 2020. Você está preparado?</p>
      </p>

      <form method="post" accept-charset="utf-8">


        <div class="form-group">
          <label class="col-md-8 control-label label-gd">Quantos clientes você atende por dia?</label>
          <div class="offset-md-2 col-md-8 inputGroupContainer">
            <input id="quantidadeDeClientes" name="quantidadeDeClientes" placeholder="Quantidade de Clientes" class="form-control" required="true" value="" type="number"></div>
        </div>
        <div class="form-group">
          <label class="col-md-8 control-label label-gd">Quais desses dados dos seus clientes ou funcionários você grava?</label>
          <div class="offset-md-2 col-md-8">
            <input class="form-check-input" type="checkbox" value="1" id="sev2" name="sev2">
            <label class="form-check-label" for="sev2">
              Data de Nascimento, CPF ou RG
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="sev1" name="sev1">
            <label class="form-check-label" for="sev1">
              Nome completo
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="sev3" name="sev3">
            <label class="form-check-label" for="sev3">
              Histórico Médico
            </label>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-8 control-label label-gd">Onde ficam gravados esses dados?</label>
          <div class="offset-md-2 col-md-8">
            <input required class="form-check-input" type="radio" value="1" id="localidadesRadio1" name="localidade">
            <label class="form-check-label" for="localidadesRadio1">
              No Excel, ou similar
            </label>
            <input required class="form-check-input" type="radio" value="2" id="localidadesRadio2" name="localidade">
            <label class="form-check-label" for="localidadesRadio2">
              Num banco de dados
            </label>
            <input required class="form-check-input" type="radio" value="0" id="localidadesRadio3" name="localidade">
            <label class="form-check-label" for="localidadesRadio3">
              Registro Físico
            </label>
            <button type="submit" class="mt-3 py-3 mx-3 btn btn-block " style="background: #4a707a" formaction='<?php echo site_url("app/generateReport") ?> '>Gerar relatório</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.slim.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>