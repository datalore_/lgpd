<?php
$this->load->view('_header');
$this->load->view('_index');
?>

<!-- Page Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h1 class="mt-5">Seu relatório está pronto</h1>
      <!--         <p class="text-muted h5">A LGPD é uma legislação que trata do bom uso dos dados dos seus clientes.
          <br />Ela entrará em efeito em 2020. Você está preparado?</p> -->
      </p>
    </div>

    <div class="paperpage offset-md-2 col-md-8" style="margin-bottom: 10vh;">
      <div style="width:100%;">
        <img src="<?php echo base_url("images"); ?>/datalore_logo_transparent.png" style="height: 15vh; float:right;" />
        <h4 class="mt-5">Sua atividade representa: <p style="width: 30%; ">Risco <?php echo $_SESSION['msg']['risco'] ?></p>
        </h4>
      </div>

      <div class="alert <?= $_SESSION['msg']['alerttype']; ?>">
        <?php

        foreach ($_SESSION['msg']['rulebreaks'] as $key => $value) {
          echo $value;
          echo '<br/>';
        }
        ?>
      </div>

      <pre>
          <?php
          echo $_SESSION['msg']['texto'];
          ?>

        </pre>
      <button class="button-lg  button" style="height: 80px; width: 50%; margin: auto; display: block; background: #4a707a;">Aprenda mais</button>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.slim.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>