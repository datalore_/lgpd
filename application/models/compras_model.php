<?php


defined('BASEPATH') or exit('No direct script access allowed');

class compras_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
    }



    public function adicionarAoCarrinho()
    {
        $qtd = 0;
        if ($this->session->hasuserdata('qtd')) {
            $qtd = $this->session->userdata('qtd');
        }
        $qtd += $_POST['qtd'];

        $this->session->setuserdata('qtd', $qtd);
    }

    public function getTotalCarrinho()
    {
        if ($this->session->hasuserdata('qtd')) {
            return $this->session->userdata('qtd');
        } else {
            return 0;
        }
    }

    public function concluirCompra()
    {
        if ($this->session->hasuserdata('qtd') && $this->session->hasuserdata('login')) {
            $data = array(
                'id_usuario' => $this->usuario->getIdUsuarioPorLogin($this->session->userdata('login')),
                'qtd' => $this->session->userdata('qtd')
            );
            return $this->db->insert($data)
                ->into('compras');


            /*         if ($rs){
            redirect('teste');
        } else {
            redirect('app');
        } */
        }
        return false;
    }
}

/* End of file compras_model.php */
