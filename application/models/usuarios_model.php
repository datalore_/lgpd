<?php


defined('BASEPATH') or exit('No direct script access allowed');

class usuarios_model extends CI_Model
{

    public function doLogin()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('login', 'Login', 'required');
        $this->form_validation->set_rules('senha', 'senha', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            $this->load->view('home');
        }
    }

    public function register(){
        if ($this->session->has_userdata('termoaceito') && $this->session->userdata('termoaceito'))
    }

    public function aceitarTermo()
    {
        $this->session->set_userdata('termoaceito', true);
    }

    public function doLogout()
    {
        session_destroy();
    }

    public function authuser()
    {
        $usr = array(
            "login" => $_POST['login'],
            "senha" => $_POST['senha']
        );
        $resultado = $this->db->get_where('users', $usr);
        $this->session->set_userdata('login', $resultado->login)
    }
}

/* End of file usuarios_model.php */
