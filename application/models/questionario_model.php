<?php

defined('BASEPATH') or exit('No direct script access allowed');

class questionario_model extends CI_Model
{

    public function rankQuestionario($data)
    {
        $data = $_POST;

        $vuln = '0';

        switch (true) {
            case $data['quantidadeDeClientes'] < 200:
                $tam = 0;
                break;
            case $data['quantidadeDeClientes'] >= 200 && $data['quantidadeDeClientes'] < 750:
                $tam = 1;
                $vuln += 0.5;
                break;
            case $data['quantidadeDeClientes'] >= 750:
                $tam = 2;
                $vuln += 1;
                break;
        }
        $msg = array();
        $rulebreaks = array();

        $rulesnippets = array();


        if (isset($data['sev1'])) {
            $vuln += 0.5;
            array_push($rulebreaks, "Alerta: Data de Nascimento, CPF ou RG - Dados Pessoais");
            array_push($rulesnippets, "You distribute must include a readable copy of this License to the terms and conditions of this License, including the files and the following manner.");
        }
        if (isset($data['sev2'])) {
            $vuln += 0.5;
            array_push($rulebreaks, "Alerta: Nome Completo - Dados Pessoais");
            array_push($rulesnippets, "You distribute must include a readable copy of this License to the terms and conditions of this License, including the files and the following manner.");
        }
        if (isset($data['sev3'])) {
            $vuln += 1;
            array_push($rulebreaks, "Alerta: Histórico Médico - Dados Pessoais Sensíveis");
            array_push($rulesnippets, "You distribute must include a readable copy of this License to the terms and conditions of this License, including the files and the following manner.");
        }

        if ($data['localidade'] == 1) {
            $vuln += 0.5;
            array_push($rulebreaks, "Alerta: Banco de dados inseguro - Perda de dados");
            array_push($rulesnippets, "You distribute must include a readable copy of this License to the terms and conditions of this License, including the files and the following manner.");
        }
        if ($data['localidade'] == 2) {
            $vuln += 1;
            array_push($rulebreaks, "Alerta: Dados Físicos - Possível perda de dados");
            array_push($rulesnippets, "You distribute must include a readable copy of this License to the terms and conditions of this License, including the files and the following manner.");
        }

        $msg['rulebreaks'] = $rulebreaks;

        $baixo = '<h5>Privacidade e Proteção de Dados</h5>
É fundamental: 
    1) o respeito à privacidade; 
    2) a autodeterminação informativa; 
    3) a liberdade de expressão, de informação, de comunicação e de opinião; 
    4) a inviolabilidade da intimidade, da honra e da imagem; 
    5) o desenvolvimento econômico e tecnológico e a inovação; 
    6) a livre iniciativa, a livre concorrência e a defesa do consumidor; e; 
    7) os direitos humanos, o livre desenvolvimento da personalidade, a dignidade e o exercício da cidadania pelas pessoas naturais.';
        $medio = '<h5>Controle de Informações</h5> 
São princípios do controle de informação:
    1) finalidade: realização do tratamento para
     propósitos legítimos, específicos, explícitos e informados ao titular, sem possibilidade de tratamento posterior de forma incompatível com essas finalidades;
    2) adequação: compatibilidade do tratamento com as finalidades informadas ao titular, de acordo com o contexto do tratamento;
    necessidade: limitação do tratamento ao mínimo necessário para a realização de suas finalidades, com abrangência dos dados pertinentes, proporcionais e não excessivos em relação às finalidades do tratamento de dados;
    3) livre acesso: garantia, aos titulares, de consulta facilitada e gratuita sobre a forma e a duração do tratamento, bem como sobre a 
    4) transparência: garantia, aos titulares, de informações claras, precisas e facilmente acessíveis sobre a realização do tratamento e os respectivos agentes de tratamento, observados os segredos comercial e industrial;
    5) segurança: utilização de medidas técnicas e administrativas aptas a proteger os dados pessoais de acessos não autorizados e de situações acidentais ou ilícitas de destruição, perda, alteração, comunicação ou difusão;';
        $alto = '<h5>Tratamento de Dados</h5>
Dados são: 
    1) Dado pessoal: informação relacionada a pessoa natural identificada ou identificável;
    2) Dado pessoal sensível: dado pessoal sobre origem racial ou étnica, convicção religiosa, opinião política, filiação a sindicato ou a organização de caráter religioso, filosófico ou político, dado referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa natural;
    3) Dado anonimizado: dado relativo a titular que não possa ser identificado, considerando a utilização de meios técnicos razoáveis e disponíveis na ocasião de seu tratamento.

Tratamento é:
    Toda operação realizada com dados pessoais, como as que se referem a coleta, produção, recepção, classificação, utilização, acesso, reprodução, transmissão, distribuição, processamento, arquivamento, armazenamento, eliminação, avaliação ou controle da informação, modificação, comunicação, transferência, difusão ou extração;';




        switch (true) {
            case $vuln >= 3:
                $msg['risco'] = 'alto';
                $msg['texto'] = $alto;
                $msg['cor'] = 'red';
                $msg['alerttype'] = 'alert-danger';
                break;
            case $vuln >= 2:
                $msg['risco'] = 'médio';
                $msg['texto'] = $medio;
                $msg['cor'] = 'yellow';
                $msg['alerttype'] = 'alert-warning';
                break;

            default:
                $msg['risco'] = 'baixo';
                $msg['texto'] = $baixo;
                $msg['cor'] = 'green';
                $msg['alerttype'] = 'alert-secondary';
                break;
        }

        return $msg;
    }
}

/* End of file questionario_model.php */
